# 作业

## 请描述什么是依赖倒置原则，为什么有时候依赖倒置原则又被称为好莱坞原则

依赖倒置原则是指高层模块不能依赖低层模块，而是都依赖抽象，抽象也不能依赖实现，而是实现依赖抽象。

具体的实现方法可以是，高层的业务逻辑层定义接口，高层面向该接口开发功能，而低层的模块实现该接口。这样做到高层不依赖低层，而是高层和低层都面向中间的接口开发。

依赖倒置原则是设计框架最重要的原则，它反转了模块之间的调用关系，框架定义接口，面向接口进行开发和设计，业务逻辑代码实现该接口，以达到与框架配合运行的效果。这也正是好莱坞原则说的，Don't call me,I'll call you。

## 请用接口隔离原则优化 Cache 类的设计，画出优化后的类图。

![题目](https://static001.geekbang.org/resource/image/9a/2c/9aec3589f51537014bed88ae21f0072c.png)

![类图](https://gitee.com/lizhikui/imgs/raw/master/2020/09/202009272310270.png)

如图，设计两个接口，按照不同的功能进行划分.

当需要使用缓存的地方，可以定义`CacheData cacheData = new Cache()`

当需要管理缓存的地方的时候可以定义`CacheManager cacheManager = new Cache()`

这样做到利用接口对功能的一个隔离。