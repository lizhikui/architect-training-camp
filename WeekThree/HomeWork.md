作业一

请在草稿纸上手写一个单例模式的实现代码，拍照提交作业。

![image-20201009181525095](https://gitee.com/lizhikui/imgs/raw/master/2020/10/202010091815250.png)

作业二

![3781039e0aefcc4c0510e44c7160c5bb](https://gitee.com/lizhikui/imgs/raw/master/2020/10/202010092202180.png)

请用组合设计模式编写程序，打印输出图 1 的窗口，窗口组件的树结构如图 2 所示，打印输出示例参考图 3。

```java
package com.lizk.knowledge.tmp;

/**
 * @author lizhikui
 * @date 2020/10/9 21:54
 */

import com.google.common.collect.Lists;

import java.util.List;

public class Window {
    /**
     * 统一的打印接口
     */
    public static interface Print {
        void print();
    }

    /**
     * 基础组件
     */
    public static class BaseWidget implements Print {
        String content;
        String widgetName;

        public BaseWidget(String content, String widgetName) {
            this.content = content;
            this.widgetName = widgetName;
        }

        @Override
        public void print() {
            System.out.println("print " + widgetName + "(" + content + ")");
        }
    }

    /**
     * 基础容器组件
     */
    public static class BaseContainer extends BaseWidget {
        List<Print> printList;

        public BaseContainer(String content, String widgetName, List<Print> printList) {
            super(content, widgetName);
            this.printList = printList;
        }

        @Override
        public void print() {
            super.print();
            printList.forEach(Print::print);
        }
    }

    //-------------以下为各种具体组件-------------

    public static class WinForm extends BaseContainer {
        public WinForm(String content, List<Print> printList) {
            super(content, "WinForm", printList);
        }
    }

    public static class Frame extends BaseContainer {
        public Frame(String content, List<Print> printList) {
            super(content, "Frame", printList);
        }
    }

    public static class Picture extends BaseWidget {
        public Picture(String content) {
            super(content, "Picture");
        }
    }

    public static class Button extends BaseWidget {
        public Button(String content) {
            super(content, "Button");
        }
    }


    public static class Lable extends BaseWidget {
        public Lable(String content) {
            super(content, "Lable");
        }
    }

    public static class TextBox extends BaseWidget {
        public TextBox(String content) {
            super(content, "TextBox");
        }
    }

    public static class PasswordBox extends BaseWidget {
        public PasswordBox(String content) {
            super(content, "PasswordBox");
        }
    }

    public static class CheckBox extends BaseWidget {
        public CheckBox(String content) {
            super(content, "CheckBox");
        }
    }

    public static class LinkLable extends BaseWidget {
        public LinkLable(String content) {
            super(content, "LinkLable");
        }
    }

    public static void main(String[] args) {
        WinForm winForm = new WinForm("WINDOW窗口",
                Lists.newArrayList(new Picture("LOGO图片"),
                        new Button("登录"),
                        new Button("注册"),
                        new Frame("FRAME1", Lists.newArrayList(
                                new Lable("用户名"),
                                new TextBox("文本框"),
                                new Lable("密码"),
                                new PasswordBox("密码框"),
                                new CheckBox("复选框"),
                                new TextBox("记住用户名"),
                                new LinkLable("忘记密码")
                        )))
        );
        winForm.print();
    }

}


```

